FROM python:3.8.0-slim
WORKDIR /app
ADD app.py /app
RUN pip install Flask
ENV NAME Mohanram
CMD ["python", "app.py"]
